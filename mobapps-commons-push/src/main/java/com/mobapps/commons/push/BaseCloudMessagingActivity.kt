package com.mobapps.commons.push

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId

abstract class BaseCloudMessagingActivity : AppCompatActivity() {

    private val isNoticiaLink: Boolean by lazy {
        pushNoticiaLink != null
    }

    private val isIndicacaoLink: Boolean by lazy {
        pushIndicacaoLink != null
    }

    private val pushNoticiaLink: String? by lazy {
        intent.getStringExtra(CloudMessaging.NOTICIA_KEY)
    }

    private val pushIndicacaoLink: String? by lazy {
        intent.getStringExtra(CloudMessaging.INDICACAO_LINK_KEY)
    }

    abstract fun openSplashScreen()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        when {
            isIndicacaoLink -> {
                pushIndicacaoLink?.let { resolveActivity(it) }
                finish()
            }
            isNoticiaLink -> {
                pushNoticiaLink?.let { resolveActivity(it) }
                openSplashScreen()
            }
            else -> openSplashScreen()
        }
    }

    fun resolveActivity(url: String){
        val newIntent = Intent(Intent.ACTION_VIEW)
        println("push - url $url")
        val urlVerified =  if (url.checkIfUrlHasProtocol())  url else "http://$url"
        println("push - urlVerified $urlVerified")
        newIntent.data = Uri.parse(urlVerified)
        if (newIntent.resolveActivity(packageManager) != null)
            startActivity(newIntent)
    }

    private fun String.checkIfUrlHasProtocol () = startsWith("https://") || startsWith("http://")

}