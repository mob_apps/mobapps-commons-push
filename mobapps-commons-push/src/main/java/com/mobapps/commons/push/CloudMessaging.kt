package com.mobapps.commons.push

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class CloudMessaging : FirebaseMessagingService() {

    private val tag = "FirebaseMessaging"
    private val channelId = "GENERAL"

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        handleOnlyDataPayload(remoteMessage.data, remoteMessage.notification?.title, remoteMessage.notification?.body)
    }

    private fun handleOnlyDataPayload(payload: MutableMap<String, String>, title: String?, body: String?) {
        if (payload.containsKey(NOTICIA_KEY)) {
            openNoticia(payload[NOTICIA_KEY], title, body)
            return
        }
        if (payload.containsKey(INDICACAO_LINK_KEY)) {
            openIndicacaoLink(payload[INDICACAO_LINK_KEY], title, body)
            return
        } else {
            openNormalNotification(title, body)
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val name = "Geral"
        val descriptionText = "Notificações gerais do app"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val mChannel = NotificationChannel(channelId, name, importance)
        mChannel.description = descriptionText
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(mChannel)
    }

    private fun openNoticia(
        link: String?,
        title: String?,
        body: String?
    ) {
        val intent = Intent(this, PushInit.launcherActivity)
        intent.putExtra(NOTICIA_KEY, link)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
        showNotification(title, body, pendingIntent)
    }


    private fun showNotification(title: String?, body: String?, pendingIntent: PendingIntent) {

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(soundUri)
            .setContentIntent(pendingIntent)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))

        PushInit.notificationIcon?.run { notificationBuilder.setSmallIcon(this) }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0, notificationBuilder.build())
    }

    private fun openIndicacaoLink(
        link: String?, title: String?,
        body: String?
    ) {
        val intent = Intent(this, PushInit.launcherActivity)
        intent.putExtra(INDICACAO_LINK_KEY, link)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
        showNotification(title, body, pendingIntent)
    }

    private fun openNormalNotification(title: String?, body: String?) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        }
        showNotification(title, body, pendingIntent)
    }

    companion object {
        const val NOTICIA_KEY = "link"
        const val INDICACAO_LINK_KEY = "app"
    }
}