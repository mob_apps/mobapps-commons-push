package com.mobapps.commons.push

import androidx.appcompat.app.AppCompatActivity

object PushInit {
    var notificationIcon: Int? = null
    var launcherActivity: Class<out BaseCloudMessagingActivity>? = null
}