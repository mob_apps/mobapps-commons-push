package com.mobapps.commons.push.samples

import android.os.Bundle
import com.igorronner.irinterstitial.init.IRAds
import com.mobapps.commons.push.BaseCloudMessagingActivity
import com.mobapps.commons.push.samples.R

class MainActivity : BaseCloudMessagingActivity() {

    private val adsInstance by lazy {
        IRAds.newInstance(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun openSplashScreen() {
        adsInstance.openSplashScreen()
    }

}
