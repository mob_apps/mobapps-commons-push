package com.mobapps.commons.push.samples

import android.app.Application
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.igorronner.irinterstitial.init.IRAdsInit
import com.igorronner.irinterstitial.init.IRBanner
import com.mobapps.commons.push.PushInit

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        PushInit.notificationIcon = R.drawable.ic_notification_small
        PushInit.launcherActivity = MainActivity::class.java
        if (BuildConfig.DEBUG) {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.w("Token_Firebase", task.result?.token, task.exception)
                        return@OnCompleteListener
                    }
                })
        }
        val adBuilder = IRAdsInit.Builder()
            .setAppId("ca-app-pub-3940256099942544~3347511713")                 // DEBUG
            .setNativeAdId("ca-app-pub-3940256099942544/2247696110")            // DEBUG
            .setInterstitialId("ca-app-pub-3940256099942544/1033173712")        // DEBUG
            .setTwoFloorsInterstitial("ca-app-pub-3940256099942544/1033173712", "ca-app-pub-3940256099942544/1033173712")
            .setTwoFloorsNativeAd("ca-app-pub-3940256099942544/2247696110", "ca-app-pub-3940256099942544/2247696110")
            .setLogo(R.mipmap.ic_launcher)
        adBuilder.build(this)
        IRBanner.initialize(this)
    }

}