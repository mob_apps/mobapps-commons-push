## MobApps Push

Para usar essa lib, você deve efetuar os seguintes passos .


## 1. Adicionar dependencias do firebase, fcm e IRAds no app
```gradle
    implementation 'com.google.firebase:firebase-core:17.3.0'
    implementation 'com.google.firebase:firebase-messaging:20.1.4'
    implementation 'org.bitbucket.mob_apps:mobapps-commons-push:0.0.1'
    implementation 'com.github.igorronner:IRAds:1.4.8'
```

O app deve estar configurado no firebase e com a lib do IRAds configurada no MainApp

## 2. Adicionar a classe do FCM no Manifest do app

Dentro da tag application, adicionar o código abaixo:

```xml
 <service
    android:name="com.mobapps.commons.push.CloudMessaging"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT" />
    </intent-filter>
</service>

<meta-data
    android:name="com.google.firebase.messaging.default_notification_icon"
    android:resource="@drawable/icone_de_notificacao_do_app" />
```

## 3. Fazer a MainActivity do app extender a BaseCloudMessagingActivity

A MainActivity do App deve ter o seguinte formato:

1. A classe deve extender BaseCloudMessagingActivity
2. O método openSplashScreen() deve ser sobescrito para chamar a splashScreen do IRAds

## 4. Incluir essa linha no MainApp para definir qual class vai ser chamada no click do push, use sempre a class que foi extendida de BaseCloudMessagingActivity

```kotlin
class MainApp : Application() {

    override fun onCreate() {
        PushInit.launcherActivity = MainActivity::class.java
    }
}
```


A MainActivity do App deve ter o seguinte formato:

1. A classe deve extender BaseCloudMessagingActivity
2. O método openSplashScreen() deve ser sobescrito para chamar a splashScreen do IRAds

## 5. Para evitar o crash -> no valid small icon <- use isso na sua class Application

```kotlin
class MainApp : Application() {

    override fun onCreate() {
        PushInit.notificationIcon = R.drawable.ic_notification_small
    }
}
```

## Atenção
A instancia do IRAds deve ser inicializada antes do onCreate se a classe estiver sendo usada em Java ou ser um lazy em Kotlin

```kotlin
class MainActivity : BaseCloudMessagingActivity() {

    private val adsInstance by lazy {
        IRAds.newInstance(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun openSplashScreen() {
        adsInstance.openSplashScreen()
    }

}
```

Após isso, o BaseCloudMessagingActivity irá fazer todo o processo para abrir os links e splash do app no onCreate